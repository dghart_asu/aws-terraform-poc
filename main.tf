# Heavily influenced by 
# https://medium.com/swlh/creating-an-instance-in-a-newly-designed-vpc-using-terraform-440a220d3886
# https://www.terraform.io/docs/providers/aws/r/instance.html

# This is where the infrastructure will be created
provider "aws" {
  region = "us-west-2"
}

# A Virtual Private Cloud (VPC) isolates the network of the resources from the rest 
# the internet. The network can only be accessed at specific gateway points.
resource "aws_vpc" "vpc" {
  cidr_block = "172.16.0.0/16"

  tags = {
    Name = "tracker-vpc"
  }
}

# This is a smaller section of the VPC that allows for other limited access
resource "aws_subnet" "subnet" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = "172.16.10.0/24"
  availability_zone = "us-west-2a"

  tags = {
    Name = "tracker-subnet"
  }
}

# This allows access to the VPC from the internet
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "tracker-internet-gateway"
  }
}

# This security group allows ssh access to the ec2 instance
# it also allows the instance to reach out to any ip
resource "aws_security_group" "sg" {
  name        = "allow_access"
  description = "Allow SSH inbound traffic"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {
    Name = "tracker-security-group"
  }
}

# An Elastic IP address is a fixed IP access that can route to
# different destinations over time. This is useful so that the
# underlaying instance can be changed out without affecting the 
# public access point. 
resource "aws_eip" "user_service_eip" {
  instance = aws_instance.user_service_ec2.id
  vpc      = true

  tags = {
    Name = "user-service-elastic-ip"
  }
}

# This attaches the Elastic IP address to the ec instance
resource "aws_eip_association" "user_service_eip_assoc" {
  instance_id   = aws_instance.user_service_ec2.id
  allocation_id = aws_eip.user_service_eip.id
}

# An Elastic IP address is a fixed IP access that can route to
# different destinations over time. This is useful so that the
# underlaying instance can be changed out without affecting the 
# public access point. 
resource "aws_eip" "tracker_api_eip" {
  instance = aws_instance.tracker_api_ec2.id
  vpc      = true

  tags = {
    Name = "tracker-api-elastic-ip"
  }
}

# This attaches the Elastic IP address to the ec instance
resource "aws_eip_association" "tracker_api_eip_assoc" {
  instance_id   = aws_instance.tracker_api_ec2.id
  allocation_id = aws_eip.tracker_api_eip.id
}

resource "aws_default_route_table" "route_table" {
  default_route_table_id = aws_vpc.vpc.default_route_table_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
  
  tags = {
    Name = "tracker-default-route-table"
  }
}

# This is a snapshot of an operating system that will be loaded on the ec2 instance
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

# This is the ec2 instance where the user service will run. 
# Although virtualized, it can be thought of as a computer
# running on a network
resource "aws_instance" "user_service_ec2" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.micro"
  key_name               = "asu-ssh-keypair"
  subnet_id              = aws_subnet.subnet.id
  vpc_security_group_ids = [aws_security_group.sg.id]

  tags = {
    Name = "user-service"
  }
}

# Another ec2 instance for the api
resource "aws_instance" "tracker_api_ec2" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.micro"
  key_name               = "asu-ssh-keypair"
  subnet_id              = aws_subnet.subnet.id
  vpc_security_group_ids = [aws_security_group.sg.id]

  tags = {
    Name = "tracker-api"
  }
}