# I wanted to use a Terraform provisoner, but didn't
# https://www.terraform.io/docs/provisioners/index.html

# The EC2 instances had no public IP because they are in the VPC
# Since the EIP was dependent on the EC2s being created first, the
# provision could never run because it couldn't connect to the instance.

sudo apt-get update -y
sudo apt-get install openjdk-11-jdk -y

# To connect to the RDS instance (not in the VPC), the EIPs need to 
# be added to the security group as inbound rules
# PostgreSQL	TCP	5432	44.225.181.35/32	-
# https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/USER_VPC.Scenarios.html#USER_VPC.Scenario5
