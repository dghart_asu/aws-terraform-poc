output "user_service_ip" {
    value = aws_eip.user_service_eip.public_ip
}

output "tracker_api_ip" {
    value = aws_eip.tracker_api_eip.public_ip
}

